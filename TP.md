PROJET JENKIS - INTEGRATION CONTINUE
====================================

Ce projet est réalisé par Clément Lavalle et Florian Ranson. Le but était de prendre un projet maven et de le lier à un jenkins.

Le contenu
==========

Récupération du projet
----------------------

Nous avons déplacé le projet github sous gitlab car nous avons plus l'habitude de l'utiliser. De plus, les autres projets jenkins que nous avons fait, sont aussi sur gitlab.

Le build
--------

Le stage de build ne nous a pas posé de problème. Nous avons simplement perdu du temps, car nous sommes récemment passé sur windows (ubuntu avant), de ce fait, il ne comprend plus le 'sh', mais le 'bat'.

Les tests
---------

Pour les tests, pas trop de soucis non plus, car dans le pom.xml d'origine, la version de junit à utiliser était déjà spécifiée.

L'analyse
---------

Pour cette partie, nous avons dû modifier le pom.xml afin de spécifier les dépendances nécessaires à l'exécution des commandes. C'est une partie que nous avions déjà fait auparavant et nous étions donc familier avec le processus d'installation.

Package
-------

De même pour le package, nous l'avions déjà utilisé, et c'est donc naturellement que nous l'avons intégré.

Les archives
------------

Nous avons décidé de garder un archive de nos pipelines. Pour cela, nous avons créer un dossier 'target' à la racine du projet, afin de venir y stocker nos snapshot. 

Les points difficiles
=====================

Nexus
-----

L'installation de nexus nous a posé beaucoup de problèmes. 

Nous avons tout d'abord installé et lancé le serveur nexus sur notre PC, jusque la tout allait bien. Nous nous sommes dit que nous allions mettre nos snapshots dans le maven-snapshots du serveur nexus. 

Nous nous sommes rendu compte que pour configurer nexus, il fallait un fichier settings.xml, pour y spécifier les identifiants de connexion à notre compte nexus.
Nous avons donc créer ce fichier dans un nouveau dossier .m2 comme la config maven le preconise. Nous avons ajouté le stage adéquat, mais c'est dans les dépendances du pom.xml que nous avons bloqué. 

Nous avons essayé beaucoup de choses différentes, corrigés beaucoup de problème que la console nous remonté, tout ces essais sont accessibles via les différents commit du projet, car à chaque commit, un build jenkins se lance. Au final, malgré toutes les recherches et les conseils que nous avons demandés sur des forums ou à des collègues, nous ne sommes pas parvenu à correctement lier le serveur nexus. 

Au final, nous nous y prenions mal, on cherchait beaucoup trop compliqué. Dans nexus, nous avons créer un nouveau repo 'project-astronomie'.
Ensuite, dans jenkins, nous avons ajouté un plugin pour gérer les artifacts avec nexus 'nexus artefact uploader' afin de pouvoir générer le code du jenkinsfile. Ensuite, dans la rubrique pipeline syntaxe de jenkins, nous avons mis les infos de notre serveur nexus, ainsi que sur les artefacts à envoyer, et les logins de connections à nexus en credentials. Avec tout cela, il nous a généré un code à mettre dans notre jenkinsfile, ce qui a fonctionné. 

De cette façon, nous avons réussi à récuperer nos snapshots dans le serveur nexus.

Nous avons tout de même laissé nos modifs dans les fichiers  pom.xml et settings.xml afin d'avoir une trace de nos premiers essais.

Sonar
-----

Enfin, nous avons essayé de brancher sonar à notre projet, afin d'avoir une analyse supplémentaire.

Nous avons donc ajouter la stage, et modifier le fichier settings.xml qui était aussi néceassaire pour configurer sonar. Malheureusement, pour sonar, c'est le serveur local qui ne veut pas se lancer. Quand nous le lançons, nous avons une erreur liée au lancement d'une JVM, nous n'avons trouvé aucune solution à ce problème.

Conclusion
==========

Ce projet et module nous a permit de comprendre le fonctionnement et l'utilité de l'intégration continue. Au travers de différents TP et ce projet, nous avons pu nous familiariser avec de nouveau outils, comme jenkins, nexus ou sonar, ainsi qu'approfondir nos connaissances sur maven.
